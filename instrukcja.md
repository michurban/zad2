# Lekcja 1 - Markdown lekki język znaczników


## Spis treści
- [Wstęp](#Wstęp)
- [Podstawy składni](#Podstawy składni)
  - [Definiowanie nagłówków](#Definiowanie nagłówków)
  - [Definiowanie list](#Definiowanie list)
  - [Wyróżnianie tekstu](#Wyróżnianie tekstu)
  - [Tabele](#Tabele)
  - [Odnośniki do zasobów](#Odnośniki do zasobów)
  - [Obrazki](#Obrazki)
  - [Kod źródłowy dla różnych wersji programowania](#Kod źródłowy dla różnych wersji programowania)
  - [Tworzenie spisu treści na podstawie nagłówków](#Tworzenie spisu treści na podstawie nagłówków)
- [Edytory dedykowane](#Edytory dedykowane)
- [Pandoc - system do kowersji dokumentów Markdown do innch formatów](#Pandoc - system do kowersji dokumentów Markdown do innch formatów)


## Wstęp 
Obecnie powszechnie wykorzystuje się języki ze znacznikami do opisania dodatkowych informacji
umieszczanych w plikach tekstowych. Z pośród najbardziej popularnych można wspomnieć o:

1. **html** – służącym do opisu struktury informacji zawartych na stronach internetowych,
2. **Tex** (Latex) – poznany na zajęciach język do „profesjonalnego” składania tekstów,
3. **XML** (Extensible Markup Language) - uniwersalnym języku znaczników przeznaczonym do reprezentowania różnych danych w ustrukturalizowany sposób.

Przykład kodu html i jego interpretacja w przeglądarce:
```
<html>
<head>
<meta charset="utf-8" />
<title>Przykład</title>
</head>
<body>
<p> Jakiś paragraf tekstu</p>
</body>
</html>
```
![image-20201027165133609](/uploads/82bce937a73e811b30368bac3f3ee1bf/image-20201027165133609.png)



Przykład kodu Latex i wygenerowanego pliku w formacie pdf
\documentclass[]{letter}
\usepackage{lipsum}
\usepackage{polyglossia}
\setmainlanguage{polish}
\begin{document}
\begin{letter}{Szanowny Panie XY}
\address{Adres do korespondencji}
\opening{}
\lipsum[2]
\signature{Nadawca}
\closing{Pozdrawiam}
\end{letter}
\end{document}

![image-20201027165317546](/uploads/0dc0cd8d3b621d7d16003a97a41a3b42/image-20201027165317546.png)

Przykład kodu XML – fragment dokumentu SVG (Scalar Vector Graphics)
<!DOCTYPE html>
<html>
<body>
<svg height="100" width="100">
<circle cx="50" cy="50" r="40" stroke="black" stroke-width="3" fill="red" />
</svg>
</body>
</html>

![image-20201027165346925](/uploads/0f1d86257537f6e95af2f2232b997a8d/image-20201027165346925.png)


W tym przypadku mamy np. znacznik np. <circle> opisujący parametry koła i który może być
właściwie zinterpretowany przez dedykowaną aplikację (np. przeglądarki www).

Jako ciekawostkę można podać fakt, że również pakiet MS Office wykorzystuje format XML do
przechowywania informacji o dodatkowych parametrach formatowania danych. Na przykład pliki z rozszerzeniem docx, to nic innego jak spakowane algorytmem zip katalogi z plikami xml.

$unzip -l test.docx
Archive: test.docx
 Length Date Time Name

--------- ---------- ----- ----
 573 2020-10-11 18:20 _rels/.rels
 731 2020-10-11 18:20 docProps/core.xml
 508 2020-10-11 18:20 docProps/app.xml
 531 2020-10-11 18:20 word/_rels/document.xml.rels
 1421 2020-10-11 18:20 word/document.xml
 2429 2020-10-11 18:20 word/styles.xml
 853 2020-10-11 18:20 word/fontTable.xml
 241 2020-10-11 18:20 word/settings.xml
 1374 2020-10-11 18:20 [Content_Types].xml

Wszystkie te języki znaczników cechują się rozbudowaną i złożoną składnią i dlatego do ich edycji
wymagają najczęściej dedykowanych narzędzi w postaci specjalizowanych edytorów. By
wyeliminować powyższą niedogodność powstał **Markdown** - uproszczony język znaczników
służący do formatowania dokumentów tekstowych (bez konieczności używania specjalizowanych narzędzi). Dokumenty w tym formacie można bardzo łatwo konwertować do wielu innych formatów: np. html, pdf, ps (postscript), epub, xml i wiele innych. Format ten jest powszechnie używany do tworzenia plików README.md (w projektach open source) i powszechnie obsługiwany przez serwery git’a. Język ten został stworzony w 2004 r. a jego twórcami byli John Gruber i Aaron Swartz. W kolejnych latach podjęto prace w celu stworzenia standardu rozwiązania i tak w 2016 r. opublikowano dokument RFC 7764 który zawiera opis kilku odmian tegoż języka:
* CommonMark,
* GitHub Flavored Markdown (GFM),
* Markdown Extra.

## Podstawy składni
Podany link:  https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)zawiera opis podstawowych elementów składni w języku angielskim. Poniżej zostanie przedstawiony ich krótki opis w języku polskim.

### Definiowanie nagłówków
W tym celu używamy znaku kratki
Lewe okno zawiera kod źródłowy – prawe -podgląd przetworzonego tekstu



![image-20201027170213911](/uploads/970fc0878c45f7eb343b9a3c57b1ca28/image-20201027170213911.png)



### Definiowanie list


![alt text](https://gitlab.com/michurban/zad2/uploads/395fda9173230142d1b6f2f2dec00591/image-20201027170259760.png)



Listy numerowane definiujemy wstawiając numery kolejnych pozycji zakończone kropką.
Listy nienumerowane definiujemy znakami: *,+,-

###  Wyróżnianie tekstu

![alt text](https://gitlab.com/michurban/zad2/uploads/5d07fc3d6a2f704c260f3a4c9f754210/image-20201027170406853.png)

### Tabele

![alt text](https://gitlab.com/michurban/zad2/uploads/0412ff999b4735804c70349093a94580/image-20201027170526524.png)



Centrowanie zawartości kolumn realizowane jest poprzez odpowiednie użycie znaku dwukropka:

### Odnośniki do zasobów
Odnośniki do zasobów
[odnośnik do zasobów](www.gazeta.pl)
[odnośnik do pliku](LICENSE.md)
[odnośnik do kolejnego zasobu][1]

[1]: http://google,com

#### Obrazki 
```
![alt text](https://server.com/images/icon48.png "Logo 1") – obrazek z zasobów
internetowych
```
```
![](logo.png) – obraz z lokalnych zasobów
```
### Kod źródłowy dla różnych języków programowania

![alt text](https://gitlab.com/michurban/zad2/uploads/349098575e1304ebaa7445e02a2932ac/image-20201027170834663.png)



### Tworzenie spisu treści na podstawie nagłówków

![image-20201027170910032](/uploads/d49f3b7dd4bf95683aa3920cae89b629/image-20201027170910032.png)

## Edytory dedykowane
Pracę nad dokumentami w formacie Markdown( rozszerzenie md) można wykonywać w
dowolnym edytorze tekstowym. Aczkolwiek istnieje wiele dedykowanych narzędzi
1. Edytor Typora - https://typora.io/
2. Visual Studio Code z wtyczką „markdown preview”

![image-20201027171021951](/uploads/5c7d1b81ed543f28cbf136365d45b121/image-20201027171021951.png)

## Pandoc – system do konwersji dokumentów Markdown do innych formatów
Jest oprogramowanie typu open source służące do konwertowania dokumentów
pomiędzy różnymi formatami.

Pod poniższym linkiem można obejrzeć przykłady użycia:
https://pandoc.org/demos.html

Oprogramowanie to można pobrać z spod adresu: https://pandoc.org/installing.html
Jeżeli chcemy konwertować do formatu latex i pdf trzeba doinstalować oprogramowanie
składu Latex (np. Na windows najlepiej sprawdzi się Miktex https://miktex.org/) 

Gdyby podczas konwersji do formatu pdf pojawił się komunikat o niemożliwości
znalezienia programu pdflatex rozwiązaniem jest wskazanie w zmiennej środowiskowej
PATH miejsca jego położenia

![image-20201027171231635](/uploads/6650b90b2a3d4a50c140150e7f173c9f/image-20201027171231635.png)

![image-20201027171247494](/uploads/8f29e942ebe0e999a811ddf0e005e0da/image-20201027171247494.png)

Pod adresem (https://gitlab.com/mniewins66/templatemn.git) znajduje się przykładowy plik
Markdown z którego można wygenerować prezentację w formacie pdf wykorzystując
klasę latexa beamer.

W tym celu należy wydać polecenie z poziomu terminala:

$pandoc templateMN.md -t beamer -o prezentacja.pdf